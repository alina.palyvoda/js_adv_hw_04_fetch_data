// Теоретичне питання

//Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// AJAX - це технологія, яка дозволяє взаємодіяти з сервером (відправляти та отримувати данні) без перезавантаження
// сторінки. Це допомагає зробити сайти більш зручними у використанні.

const url = "https://ajax.test-danit.com/api/swapi/films";


fetch(url)
    .then((response) => {
        return response.json();
    })
    .then((res) => {
        res.forEach((({episodeId, name, openingCrawl, characters}) => {
            document.querySelector(".container")
                .insertAdjacentHTML("beforeend", `<div class="episode_${episodeId}">
        <h4>Episode ${episodeId} "${name}"</h4>
        <p>${openingCrawl}</p>
        <h5 class="episode_${episodeId}__characters">Characters:</h5>
        </div>`);
            characters.forEach(el => {
                fetch(el).then((response) => {
                    return response.json();
                })
                    .then((res) => {
                        document.querySelector(`.episode_${episodeId}__characters`)
                            .insertAdjacentHTML("beforeend", `<ul class="character">
                        ${res.name} 
                        <li>gender: ${res.gender}</li> 
                        <li>birth year: ${res.birthYear}</li>
                        <li>eye color: ${res.eyeColor}</li>
                        <li>hair color: ${res.hairColor}</li>
                        <li>skin color: ${res.skinColor}</li>
                        <li>height: ${res.height}</li>
                        <li>mass: ${res.mass}</li>
                        </ul>`)
                    })
            })
        }))
    });
